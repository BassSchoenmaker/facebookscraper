import argparse
import time
import json
import csv

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from datetime import datetime

delay = 10 # seconds

def extract(page, numOfPost, infinite_scroll=False, scrape_comment=False, keywords=""):
    with open('./Needs/facebook_credentials.txt') as file:
        email = file.readline().split('"')[1]
        password = file.readline().split('"')[1]

    option = Options()

    option.add_argument("--disable-infobars")
    option.add_argument("start-maximized")
    option.add_argument("--disable-extensions")
    # option.add_argument("--headless")
    # option.add_argument('--disable-gpu')

    # Pass the argument 1 to allow and 2 to block
    option.add_experimental_option("prefs", {
        "profile.default_content_setting_values.notifications": 1
    })

    browser = webdriver.Chrome(executable_path="./chromedriver", options=option)
    browser.get("http://facebook.com")
    browser.maximize_window()
    browser.find_element_by_name("email").send_keys(email)
    browser.find_element_by_name("pass").send_keys(password)
    browser.find_element_by_id('loginbutton').click()
    time.sleep(1)
    browser.get("http://facebook.com/" + page + "/posts")
    try:
        searchbar = WebDriverWait(browser, delay).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'input._58al')))
        print(searchbar)
    except TimeoutException:
        print("Loading SEARCHBAR took too much time!")
    browser.find_element_by_css_selector('input._58al').send_keys(keywords)
    browser.find_element_by_css_selector("button._3fbq._4jy0._4jy3._517h._51sy._42ft").click()
    postAmount = 8;
    # no reload so waiting is required
    time.sleep(5)
    match = False
    while not match:
        if postAmount < int(numOfPost):
            try:
                time.sleep(1)
                myElem = WebDriverWait(browser, delay).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'a.pam.uiBoxLightblue.uiMorePagerPrimary')))
                browser.find_element_by_css_selector('a.pam.uiBoxLightblue.uiMorePagerPrimary').click()
                postAmount += 8;
            except TimeoutException:
                print("Loading MORE LINKS took too much time!")
        else:
            match = True

    links = []
    try:
        myElem = WebDriverWait(browser, delay).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, 'div[data-highlight-tokens]._5zwe._1ch4')))
    except TimeoutException:
        print("Loading FILTERD POSTS took too much time!")

    elements = browser.find_elements_by_css_selector('div[data-highlight-tokens]._5bl2._3u1._41je._440e._87m1')
    print(len(elements))
    for element in elements:
        try:
            div = element.find_element_by_css_selector('div._o02 div._307z div[data-ft] div._78jz div._78j- div._78k7 span')
            a = div.find_element_by_tag_name('a')
            print(a.get_attribute("href"))
            links.append(a.get_attribute("href"))
        except :
            print("no comments on post skipping")
    print(links)
    data = []
    for link in links:
        browser.get(link)
        try:
            myElem = WebDriverWait(browser, delay).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, 'div[data-testid=post_message]')))
            div = browser.find_element_by_css_selector("div[data-testid=post_message]")
            postText = ""
            postText = div.find_element_by_tag_name("p").get_attribute('innerHTML')
        except TimeoutException:
            print("Loading POST took too much time!")
            continue
        # get Date
        date = browser.find_element_by_tag_name("abbr").get_attribute('title')

        # get Post link
        postLink = browser.find_element_by_css_selector("a[aria-label]._52c6")
        postLinkUrl = postLink.get_attribute("href")
        postLinkText = postLink.get_attribute("aria-label")
        # get shares
        shares = 0
        try:
            myElem = WebDriverWait(browser, delay).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'a._3rwx._42ft')))
            div = browser.find_element_by_css_selector("a._3rwx._42ft")
            shares = div.get_attribute('innerHTML')
        except TimeoutException:
            print("Loading SHARES took too much time!")
        # get comments
        comments = []
        try:
            myElem = WebDriverWait(browser, delay).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'ul._7791')))
            ulElement = browser.find_element_by_css_selector("ul._7791")
        except TimeoutException:
            print("Loading COMMENTS took too much time!")
            # print(ulElement)
        commentSpans = ulElement.find_elements_by_css_selector("span span._3l3x span")
        # print(commentSpans)
        for span in commentSpans:
            comments.append(span.text)
            print(span.text)
        post = {
            "url": link,
            "date": datetime.strptime(date, "%A, %B %d, %Y at %I:%M %p"),
            "text": postText,
            "textLink": postLinkText,
            "link": postLinkUrl,
            "shares": shares,
            "commentAmount": len(comments),
            "comments": comments,
                 }
        # print(post)
        data.append(post)

    print(data)
    browser.close()

    return data


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Facebook Page Scraper")
    required_parser = parser.add_argument_group("required arguments")
    required_parser.add_argument('-page', '-p', help="The Facebook Public Page you want to scrape", required=True)
    required_parser.add_argument('-len', '-l', help="Number of Posts you want to scrape", type=int, required=True)
    optional_parser = parser.add_argument_group("optional arguments")
    optional_parser.add_argument('-infinite', '-i',
                                 help="Scroll until the end of the page (1 = infinite) (Default is 0)", type=int,
                                 default=0)
    optional_parser.add_argument('-usage', '-u', help="What to do with the data: "
                                                      "Print on Screen (PS), "
                                                      "Write to Text File (WT) (Default is WT)", default="CSV")

    optional_parser.add_argument('-comments', '-c', help="Scrape ALL Comments of Posts (y/n) (Default is n). When "
                                                         "enabled for pages where there are a lot of comments it can "
                                                         "take a while", default="No")
    optional_parser.add_argument('-keywords', '-k', help="the words that get put in de searchbar")
    args = parser.parse_args()

    infinite = False
    if args.infinite == 1:
        infinite = True

    scrape_comment = False
    if args.comments == 'y':
        scrape_comment = True

    keywords = ""
    if args.keywords:
        keywords = args.keywords

    postBigDict = extract(page=args.page, numOfPost=args.len, infinite_scroll=infinite, scrape_comment=scrape_comment, keywords=keywords)

    if args.usage == "WT":
        with open('output.txt', 'w') as file:
            for post in postBigDict:
                file.write(json.dumps(post))  # use json load to recover

    elif args.usage == "CSV":
        with open(args.page+'.csv', 'w',) as csvfile:
           writer = csv.writer(csvfile)
           #writer.writerow(['Post', 'Link', 'Image', 'Comments', 'Reaction'])
           writer.writerow(['url','date', 'text', 'textLink','link', 'shares','commentAmount', 'comments'])

           for post in postBigDict:
              writer.writerow([post['url'],post['date'], post['text'],post['textLink'], post['link'], post['shares'],post['commentAmount'], post['comments']])
              #writer.writerow([post['Post'], post['Link'],post['Image'], post['Comments'], post['Reaction']])

    else:
        for post in postBigDict:
            print("\n")

    print("Finished")
